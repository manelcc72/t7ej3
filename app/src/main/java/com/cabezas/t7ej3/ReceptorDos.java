package com.cabezas.t7ej3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ReceptorDos extends BroadcastReceiver {
    public ReceptorDos() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i=new Intent("com.imaginagroup.LANZA_ACTIVITY_DOS");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
